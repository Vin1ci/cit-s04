package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7758676644158652494L;
	
	public void init() throws ServletException{
		System.out.println("***************");
		System.out.println(" DetailsServlet has been initialized");
		System.out.println("***************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException{
		
		//Servlet Context
		ServletContext srvContext = getServletContext();
		
		//Servlet Context parameter used to retrieve data from the xml file
		String branding = srvContext.getInitParameter("branding");
		
		//Retrieves first name
        String fname = System.getProperty("fname");

        //Retrieves last name
        HttpSession session = req.getSession();
        String lname = (String)session.getAttribute("lname");

        //Retrieves email
        String email = (String) srvContext.getAttribute("email");

        //Retrieves contact  
        String contact = req.getParameter("contact");
		
		//Prints all the retrieved data using different methods
		PrintWriter out = res.getWriter();
		out.println("<h1>Welcome to Phonebook "+branding+"</h1>"+ 
					"<p>First Name: "+ fname +"</p>"+ 
					"<p>Last Name: "+ lname +"</p>"+ 
					"<p>Contact: "+ contact +"</p>"+ 
					"<p>Email: "+ email +"</p>");
	}
	
	public void destroy() {
		System.out.println("***************");
		System.out.println(" DetailsServlet has been destroyed");
		System.out.println("***************");
	}
}
