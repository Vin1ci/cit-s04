package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7560556849380195433L;
	
	private ArrayList<String> data;
	
	public void init() throws ServletException{
		System.out.println("***************");
		System.out.println(" UserServlet has been initialized");
		System.out.println("***************");
	}
	
		//Data gathered from diff sources
		//Servlet that handles the post request from the form
		public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
			
		    //Input from request parameters
		    String lname = req.getParameter("lname");
		    String email = req.getParameter("email");
		    String contact = req.getParameter("contact");
				
		    //System Property method for first name
		    System.getProperties().put("fname", req.getParameter("fname"));
		    
		    //Http Session method for last name
		    HttpSession session = req.getSession();
		    session.setAttribute("lname", lname);
				
		    //Servlet Context setAttribute method for email	
		    ServletContext srvContext = getServletContext();
		    srvContext.setAttribute("email", email);
			
		    //URL rewriting via sendRedirect for contact	
		    res.sendRedirect("details?contact=" +contact);  
		    
		}
	
	public void destroy() {
		System.out.println("***************");
		System.out.println(" UserServlet has been destroyed");
		System.out.println("***************");
	}
}
